$(document).ready(function() {
    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function() {};

    $(".file_button, .command_button .gcode_button").click(function() {
        if ( ! $( this ).hasClass( "disabled" ) ) {
            process_button($(this));
        } 
        return false;
    });
    
    updater.start();
});


function process_button(button) {
    console.log ( button.text() );
    console.log ( button );


    var message = {};
    
    if ( button.hasClass( "file_button" ) ){
        message["command"] = "load print";
        message["parameters"] = button.text();
    } else if ( button.hasClass( "command_button" ) ){
        switch ( button.attr('id') ){
            case "print":
                message["command"] = "start print";
                break;
            case "pause":
                message["command"] = "pause print";
                break;
        }
    }
    updater.socket.send(JSON.stringify(message));
}

function process_status(message) {
    console.log ( "updating status" );
    console.log ( message.status );
    
    switch (message.status ){
        case "Initializing...":
            break;
        case "printing":
            $("#print").addClass("disabled")
            $("#pause").removeClass("disabled")
            break;
        case "paused":
            $("#print").removeClass("disabled")
            $("#pause").addClass("disabled")
            break;
        case "file loaded":
            $(".file_button").addClass("disabled")
            $("#print").removeClass("disabled")
            break;
        default:
            
    }
    $("#status").text(message.status);
    $("#status").closest( "p" ).effect("highlight", {}, 3000);
    
}