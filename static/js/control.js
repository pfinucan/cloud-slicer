$(document).ready(function() {
    if (!window.console) window.console = {};
    if (!window.console.log) window.console.log = function() {};

    $("#gcodeform").on("submit", function() {
        newMessage($(this));
        return false;
    });
    $("#gcodeform").on("keypress", function(e) {
        if (e.keyCode == 13) {
            newMessage($(this));
            return false;
        }
    });
    $(".file_button, .command_button, .gcode_button").click(function() {
        if ( ! $( this ).hasClass( "disabled" ) ) {
            process_button($(this));
        } 
        return false;
    });
    
    $("#message").select();
    updater.start();
});

function process_button(button) {
    console.log ( button.text() );
    console.log ( button );
    console.log ( button.attr('id') );
    
    var message = {};
    
    if ( button.hasClass( "file_button" ) ){
        message["command"] = "load print";
        message["parameters"] = button.text();
    } else if ( button.hasClass( "command_button" ) ){
        switch ( button.attr('id') ){
            case "start_pt":
                message["command"] = "start passthrough";
                break;
            case "end_pt":
                message["command"] = "stop passthrough";
                break; 
        }
    } else if ( button.hasClass( "gcode_button" ) ){
        switch ( button.attr('id') ){
            case "code":
                console.log ( "### Here ###" );
                console.log ( button.data('gcode') );
                message["command"] = "gcode";
                message["parameters"] = button.data('gcode');
                break;
            case "bump":
                message["command"] = "gcode";
                message["parameters"] = "G91";
                updater.socket.send(JSON.stringify(message));
                
                message["command"] = "gcode";
                message["parameters"] = "G1 Z" + button.data('gcode');
                
                updater.socket.send(JSON.stringify(message));
                
                message["command"] = "gcode";
                message["parameters"] = "G90";
                break;
            default:
                message["command"] = "gcode";
                message["parameters"] = button.attr('id');
            
        }
    }
    updater.socket.send(JSON.stringify(message));
}

function newMessage(form) {
    try{
        var command = form.formToDict();
        console.log ( command );
        
        var message = {};
        message["command"] = "gcode";
        message["parameters"] = command["gcode"];
        
        updater.socket.send(JSON.stringify(message));
        //form.find("input[type=text]").val("").select();
        $("input[name=command]").val("").select();
    }
    catch(err) {
        console.log ( err );
    }
}

function send_command(button, command) {
    console.log ( button[0].id );
	console.log ( command);

        var message = {};
        message["command"] = command;
        updater.socket.send(JSON.stringify(message));
}


jQuery.fn.formToDict = function() {
    var json = {} 
    var fields = this.serializeArray();    
    for (var i = 0; i < fields.length; i++) {
        json[fields[i].name] = fields[i].value;
    }
    if (json.next) delete json.next;
    return json;
};

var updater = {
    socket: null,

    start: function() {
        var url = "ws://" + location.host + "/chatsocket";
        updater.socket = new WebSocket(url);
        updater.socket.onmessage = function(event) {
            updater.showMessage(JSON.parse(event.data));
        }
    },

    showMessage: function(message) {
        console.log ( message );
        //var existing = $("#m" + message.id);
        //if (existing.length > 0) return;
        if (message.incoming) {
            var node = $("<p class='message_small'>" + message.incoming + "</p>");
            node.hide();
            $("#incoming").append(node);
            node.slideDown();
            $("#incoming").scrollTop($("#incoming").get(0).scrollHeight+500);
        }
        if (message.outgoing) {
            var node = $("<p class='message_small'>" + message.outgoing + "</p>");
            node.hide();
            $("#outgoing").append(node);
            node.slideDown();
            $("#outgoing").scrollTop($("#outgoing").get(0).scrollHeight+500);
            //$("#outgoing").animate({scrollTop: 1000}, 500);
        }
    }
};
