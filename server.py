#!/usr/bin/env python
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""A python Tornado server with an extra internal socket manager


"""
import errno
import functools
import socket
import logging
import tornado.escape
import tornado.ioloop
import tornado.iostream
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid
import json
import ip
import db
import gcode
import slic3r

from tornado.options import define, options

connections = set()
websockets = set()

define("port", default=80, help="run on the given port", type=int)
__UPLOADS__ = os.path.join(os.path.dirname(__file__), "prints/")
logging.info("server uploads path is %s", __UPLOADS__)

class Application(tornado.web.Application):
    """Tornado main application class, adds handlers to each page and the 
    websocket class
    """
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/chatsocket", WebSocket),
            (r"/credits", Credits),
            (r"/upload", Upload),
            (r"/file/([^/]+)", PrintFiles),
            (r"/download/([^/]+)", DownLoadGcode),
            (r"/data", AllData),
        ]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        
        tornado.web.Application.__init__(self, handlers, **settings)


class MainHandler(tornado.web.RequestHandler):
    """Web server 'index' page 
    """
    def get(self):
        #self.render("index.html", messages=WebSocket.cache)
        for connection in connections:
            #pass
            #print dir(connection)
            connection.send_data({"info":"someone connected"})
        files = [{"id":file.id, "name":file.name,"date":file.created_date.strftime("%m/%d/%Y %H:%M")} for file in db.Files.select()]
        self.render("index.html", messages="", files=files)
        
    def post(self):
        try:
            fileinfo = self.request.files['filearg'][0]
            #print self.request.files
            #print "fileinfo is", fileinfo.keys
            fname = fileinfo['filename']
            extn = os.path.splitext(fname)[1]
            logging.info("uploaded file extension is %s", extn)
            
            if extn in ['.stl' ]:
                cname = str(uuid.uuid4()) + ".stl"
                file_path = __UPLOADS__ + cname
                fh = open(file_path, 'w')
                fh.write(fileinfo['body'])
                details = {'uuid': cname,
                           'name': fname,
                           'file_location': "cloud",
                           'file_path': file_path,
                           'file_mtime': os.path.getmtime(file_path),
                           'file_size': os.path.getsize(file_path),
                          }
                
                logging.info("file details %s", details)                
                file = db.Files.create(**details)
                message = ["Upload successful",]

                self.redirect("/file/" + str(file.id))

            else:
                
                message = ["Upload failed",]
        except Exception, e:
            #logging.error("File upload failed", e)  
            print e
            message = ["Upload Failed",]
            files =[]  
            

        files = [{"name":file.name,
                  "changed":file.file_mtime,
                  "size":file.file_size,
                  "date":file.created_date.strftime("%m/%d/%Y %H:%M")} for file in db.Files.select()]
        self.render("fileuploadform.html", messages=message, files=files)
        

class Upload(tornado.web.RequestHandler):
    """Web server upload page
    """
    def get(self):
       
        files = [{"name":file.name,"date":file.created_date.strftime("%m/%d/%Y %H:%M")} for file in db.Files.select()]
        self.render("fileuploadform.html", messages="", files=files)
        
    def post(self):
        try:
            fileinfo = self.request.files['filearg'][0]
            #print self.request.files
            #print "fileinfo is", fileinfo.keys
            fname = fileinfo['filename']
            extn = os.path.splitext(fname)[1]
            logging.info("uploaded file extension is %s", extn)
            
            if extn in ['.stl' ]:
                cname = str(uuid.uuid4())
                file_path = __UPLOADS__ + cname
                fh = open(file_path, 'w')
                fh.write(fileinfo['body'])
                details = {'uuid': cname,
                           'name': fname,
                           'file_location': "cloud",
                           'file_path': file_path,
                           'file_mtime': os.path.getmtime(file_path),
                           'file_size': os.path.getsize(file_path),
                          }
                
                logging.info("file details %s", details)                
                file = db.Files.create(**details)
                message = ["Upload successful",]

            else:
                
                message = ["Upload failed",]
        except Exception, e:
            #logging.error("File upload failed", e)  
            print e
            message = ["Upload Failed",]
            files =[]  
            

        files = [{"name":file.name,
                  "changed":file.file_mtime,
                  "size":file.file_size,
                  "date":file.created_date.strftime("%m/%d/%Y %H:%M")} for file in db.Files.select()]
        self.render("fileuploadform.html", messages=message, files=files)


class Credits(tornado.web.RequestHandler):
    """Web server Settings page
    """
    def get(self):
        for connection in connections:
            connection.send_data({"command":"send settings"})
        settings = db.Settings.select()
        logging.debug("Settings: %s", settings) 
        messages = {}
        messages['ip'] = ip.get_lan_ip()
        self.render("settings.html", messages=messages, settings = settings)
        
        
class PrintFiles(tornado.web.RequestHandler):
    """Web server Settings page
    """
    def get(self, slug):
        print "slug: ",
        print slug
        logging.info("single file page: %s", slug)

        messages = {}
        file_details = None
        file_id = slug
        file_records = db.Files
        single_file = file_records.get(file_records.id == file_id)
        #print single_file.dict()
        try:
            file_details = db.PrintFileDetails.get(db.PrintFileDetails.file_id == single_file)
        
            print file_details
        except:
            pass
        #print single_file
        self.render("single_file.html", messages=messages, 
                    file = single_file, 
                    file_details = file_details)
        

class DownLoadGcode(tornado.web.RequestHandler):
    """Web server Settings page
    """       
    def get(self, slug):
        logging.info("Download parameters: %s", slug) 
        
        file_name = 'gcode/135ad8df-d65a-420e-bd9f-984e694a17c1.stl.gcode'
        buf_size = 4096
        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition', 'attachment; filename=' + file_name)
        with open(file_name, 'r') as f:
            while True:
                data = f.read(buf_size)
                if not data:
                    break
                self.write(data)
        self.finish()
        
        
class AllData(tornado.web.RequestHandler):
    """Database dumb page
    """
    def get(self):
        messages = {}
        data = {}
        data['settings'] = db.Settings.select()
        data['files'] = db.Files.select()
        data['print_file_details'] = db.PrintFileDetails.select()
        
        prints = db.Prints.select()
        data['prints'] = prints
        prints_details = {}
        data_points = db.DataPoints
        for single_print in prints:
            prints_details[single_print] = []
            for data_point in data_points.select().where(data_points.print_file == single_print):
                prints_details[single_print].append(data_point.dict())
        
        #print prints_details
        
        logging.info("Database dump", data) 
        logging.info("prints_details dump", prints_details) 
        self.render("db.html", prints_details = prints_details, messages = messages, data = data)


class WebSocket(tornado.websocket.WebSocketHandler):
    """Web socket controller class
    """
    waiters = set()
    cache = []
    cache_size = 200

    def allow_draft76(self):
        # for iOS 5.0 Safari
        return True

    def open(self):
        logging.info("websocket Open")
        self.id = str(uuid.uuid4())[:5]  # create a short id
        message = {
            "id": self.id,
            "body": "This is your id",
            "from": "info",
            }
        #self.write_message(chat)
        #print (self.request)
        self.ip = self.request.remote_ip
        self.write_message(message)
        WebSocket.waiters.add(self)

    def on_close(self):
        WebSocket.waiters.remove(self)

    @classmethod
    def update_cache(cls, chat):
        cls.cache.append(chat)
        if len(cls.cache) > cls.cache_size:
            cls.cache = cls.cache[-cls.cache_size:]

    @classmethod
    def send_updates(cls, chat):
        logging.info("sending message to %d waiters", len(cls.waiters))
        for waiter in cls.waiters:
            try:
                logging.debug("waiter id: %s, waiter ip: %s", waiter.id, waiter.ip )
                waiter.write_message(chat)
            except:
                logging.error("Error sending message", exc_info=True)

    def on_message(self, message):
        logging.info("got message %r", message)
        #print( message )
        logging.info("request ip: %s", self.request.remote_ip)
        parsed = tornado.escape.json_decode(message)
        if "command" in parsed:
            try:
                logging.info("parameters: %s", parsed["parameters"])
                file = db.Files.get(uuid=parsed["parameters"])
                logging.info("file uuid: %s", file.uuid)   
                slicer = slic3r.Slic3r(WebSocket)
            except Exception, e:
                logging.error("file not found in db, error: %s", e) 
            
            if parsed["command"] == "info":
                logging.info("server info code starting...")
                slicer.part_info(file.uuid)
                logging.error("Slice details: %s", slicer.print_details)
                details = slicer.print_details
                details['file_id'] = file
                
                info = db.PrintFileDetails.create(**details)
                print info
                
            if parsed["command"] == "slice":
                logging.info("server slice code starting...")
                slicer.slice(file.uuid)
                logging.info("server excute starting...")
                slicer.execute()
                

            for connection in connections:
                connection.send_data(parsed)
        else:
            try: 
                logging.info("message from %s", parsed["from"])
            except:
                pass
            chat = {
                "id": str(uuid.uuid4()),
                "body": parsed["body"],
                "from": self.id,
                "to": parsed["to"]
                }
            chat["html"] = tornado.escape.to_basestring(
                self.render_string("message.html", message=chat))
            
            WebSocket.update_cache(chat)
            WebSocket.send_updates(chat)



def main():

    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    io_loop = tornado.ioloop.IOLoop.instance()

    try:
        io_loop.start()
    except KeyboardInterrupt:
        io_loop.stop()
        print "exited cleanly"    


if __name__ == "__main__":
    main()
