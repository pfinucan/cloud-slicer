#!/usr/bin/env python
#
import sys
import fileinput
import os
import sys
import re
import math
import datetime


FILE_BASE = "C:/Users/patri_000/Documents/re3D/products/GUI/software/app"
FILEPATH = "/uploads/"


class GCode(object):
    def __init__(self, file_path=""):
        self.file_path = file_path
        self.file_content = []
        self.layers = []
        self.layer_count = 0
        self.line_number = 0
        self.current_feedrate = 0
        #self.path = os.path.dirname( os.path.abspath(__file__) ) 
        self.filament_use = 0.0
        self.positive = {'X': 0.0, 'Y': 0.0, 'Z': 0.0, 'E': 0.0, 'F': 0.0}
        self.total_size = {'Xmin': None, 'Xmax': None, 'Ymin': None, 'Ymax': None, 'Zmin': 0.0, 'Zmax': None}
        self.total_time = 0
        self.formated_time = None
        self.total_distance = 0
        
        self.gcodes = { "G0":   self.G0,
                        "G1":   self.G1,
                        "G3":   self.G1,
                        "G4":   self.G1,
                        "G92":  self.G92,
                      }
    
    def load(self):
        self.file_content = []
        self.line_number = 0
        try:
            self.gcode_file = open(self.file_path)
            self.gcode_file_name = self.file_path
            with self.gcode_file as f:
                for line in f:
                    #if len(self.file_content) > 200:
                    #    break
                    fixed_line = self.gcode_dispatch(line)
                    if fixed_line:
                        self.file_content.append(fixed_line)
            self.gcode_file.close()
            output = {}
            output['duration'] = self.convert_time(self.total_time)
            output['distance'] = self.total_distance
            output['filament'] = self.filament_use
            output['print_time'] = self.convert_time(self.total_time)
            output['layers'] = len( self.layers )
            
            for axis in self.total_size:
                output[axis.lower()] = self.total_size[axis]
            
            print output

            return output
        except IOError, e:
            print e
            print 'failed to open file: ' + self.file_path
            
    def gcode_dispatch(self, gcode):
        """Simple gcode line conditioner. Currently just removes comments
        """
        comment = ";"
        fixed = gcode.strip().split(comment,1)[0]
        
        # This is a dictionary switch statement
        args = fixed.split(' ')
        try:
            self.gcodes[args[0]](args)
        except KeyError:
            #skip any ramdom gcodes not defined yet
            pass
        
        return fixed
        
    def G0(self, line):
        pass
        
    def G1(self, args):
        v = {"X": None, "Y": None, "Z": None, "E": None, "F": None}
        args.pop(0)
        distance = 0
        line_feedrate = self.current_feedrate
        for arg in args:
            v[arg[0]] = float(arg[1:])
 
        for axis in ['X','Y','Z']:
            if v[axis]:
                distance += (self.positive[axis] - v[axis])**2 
                
                if not self.total_size[axis + "min"]:
                    self.total_size[axis + "min"] = v[axis]
                if not self.total_size[axis + "max"]:
                    self.total_size[axis + "max"] = v[axis]
                if v[axis] < self.total_size[axis + "min"]:
                    self.total_size[axis + "min"] = v[axis]
                if v[axis] > self.total_size[axis + "max"]:
                    self.total_size[axis + "max"] = v[axis] 
                self.positive[axis] = v[axis]
        distance = (distance)**0.5
                
        if v['E']: 
            E_delta = v['E'] - self.positive['E']
            if E_delta != 0:
                self.filament_use += E_delta
            
            # Note lines with extrusions, record as a layer
            if self.positive['Z'] not in self.layers:
                self.layers.append(self.positive['Z'])
            self.positive['E'] = v['E'] 

        if v['F']:
            if float(v['F']) != self.current_feedrate:
                line_feedrate = (float(v['F']) + self.current_feedrate) / 2
            self.current_feedrate = float(v['F'])
        move_time = distance / line_feedrate
        #print move_time
        self.total_time += move_time
        self.total_distance += distance
        
    def G92(self, args):
        # Zero out all axis
        if len(args) == 1:
            for arg in ['X', 'Y', 'Z', 'E']:
                self.positive[arg] = 0.0
        args.pop(0)
        for arg in args:
            try:
                self.positive[arg[0]] = float(arg[1:])
            except KeyError:
                print "Unknown arg being updated"
    
    def convert_time(self, minutes):
        seconds = minutes * 60
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        return "%d:%d:%02d:%02d" % (d, h, m, s)
        
        
if __name__ == "__main__":
    #file_path = FILE_BASE + FILEPATH + "6a21f1fd-e203-475f-9557-31bf491c9460.gcode"
    file_path = "C:/Users/patri_000/Documents/re3D/prints/misc/loubie_aria_dragon_300_v2.gcode"
    #file_path = FILE_BASE + FILEPATH + "5ab0d157-b660-4405-8c3b-c8fccdf70472.gcode"
    
    gcode = GCode(file_path=file_path)
    gcode.load()
    
    