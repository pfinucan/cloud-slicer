import subprocess
import os
import sys

class ProcessException(Exception):
    pass

class Slic3r():
    """Add comment soon test"""
    
    def __init__(self, websocket):
        self.websocket = websocket
        self.base_path = os.path.dirname(os.path.abspath(__file__))
        self.file_path = "/home/patrick_finucane_gmail_com/cloud-slicer/prints/"
        self.path_out = "/home/patrick_finucane_gmail_com/cloud-slicer/gcode/"
        self.setting_path = "/home/patrick_finucane_gmail_com/cloud-slicer/settings/"
        self.settings_file = "Gigabot_Slic3r_config.ini"
        print self.file_path
        print os.path.exists(self.file_path)
        self.file_base_name = None
        self.command = ["/home/patrick_finucane_gmail_com/Slic3r/slic3r.pl"]
        self.print_details = {'file_id': None, 'x': None, 'y': None, 'z': None, 
                              'facets': None, 'shells': None, 'volume': None,
                              'repair': None}
        
    def add_option(self, option, command):
        self.command.append( option )
        self.command.append( command )
        print self.command
    
    def slice(self, file_name):
        self.file_base_name = os.path.basename(file_name)
        
        print self.file_base_name
        
        self.command.append( self.file_path + file_name )
        self.command.append('--load')
        self.command.append( self.setting_path + self.settings_file)
        self.command.append('--output')
        self.command.append( self.path_out + self.file_base_name + '.gcode')
        
        #self.execute(self.command)
        
    def part_info(self, file_name):
        self.file_base_name = os.path.basename(file_name)
        
        print self.file_base_name
        
        self.command.append('--info')
        self.command.append( self.file_path + file_name )
        
        print self.command
        self.execute(self.command)

    def execute(self, command = None):
        print "command exists: ",
        if not command: 
            command = self.command
        print command

        process = subprocess.Popen(command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # Poll process for new output until finished
        while True:
            nextline = process.stdout.readline()
            
            if nextline == '' and process.poll() != None:
                break
            sys.stdout.write(nextline)
            sys.stdout.flush()
            self.parse_line(nextline)
            if nextline.strip():
                print nextline
                self.websocket.send_updates({'incoming': nextline})
        output = process.communicate()[0]
        exitCode = process.returncode
        if (exitCode == 0):
            return output
        else:
            #raise ProcessException(command, exitCode, output)
            pass
            
    def parse_line(self, line):
        key_words = ['size:','number of facets:','number of shells:','volume:','needed repair:']
        key_match = {'size:': None,
                     'number of facets:':'facets',
                     'number of shells:': 'shells',
                     'volume:': 'volume' ,
                     'needed repair:': 'repair',
                     }
        for key_word in key_words:
            if key_word in line:
                line_array = line.split(' ')
                print line_array
                if key_word == 'size:':
                    x = line_array[-3][2:]
                    y = line_array[-2][2:]
                    z = line_array[-1][2:]
                    self.print_details['x'] = x.strip()
                    self.print_details['y'] = y.strip()
                    self.print_details['z'] = z.strip()
                elif key_word == 'needed repair:':
                    repairs = line_array[-1].strip()
                    print "REPAIR LINE: ",
                    print repairs
                    if repairs == 'no':
                        self.print_details['repair'] = False
                    elif repairs == 'yes':
                        self.print_details['repair'] = True
                    else:
                        print "ERROR pasing repair Field"
   
                else:
                    self.print_details[key_match[key_word]] = line_array[-1].strip()
                    
                print "found keyword ",
                print key_word
                
            
if __name__ == "__main__":

    slicer = Slic3r()
    
    print_file = "b0cd4f3e-5701-40e4-8ffb-0b64139c2db2.stl"
    slicer.slice(print_file)
    print "done"
